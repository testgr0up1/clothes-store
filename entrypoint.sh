#!/bin/bash

# Ожидание запуска базы данных
while ! nc -z db 5432; do
    sleep 0.1
done

# Запуск приложения
python manage.py runserver 0.0.0.0:8000
