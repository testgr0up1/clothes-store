# Установка образа
FROM python:3.9-slim-buster

# Установка переменной окружения для Python
ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

# Установка зависимостей приложения
RUN apt-get update && \
    apt-get -y install netcat && \
    pip install --upgrade pip

COPY requirements.txt /requirements.txt
RUN pip install -r /requirements.txt

# Создание директории приложения в контейнере
RUN mkdir /app

# Определение рабочей директории в контейнере
WORKDIR /app

# Копирование исходного кода приложения в контейнер
COPY . /app/

# Запуск миграций базы данных
RUN python manage.py migrate

# Копирование скрипта запуска приложения в контейнер
COPY ./entrypoint.sh /entrypoint.sh

# Добавление прав на выполнение скрипта
RUN chmod +x /entrypoint.sh

# Запуск приложения
CMD ["/entrypoint.sh"]
